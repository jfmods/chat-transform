package io.gitlab.jfronny.chattransform.client;

public class TransformStart {
    private boolean available = false;
    private int value = -1;
    private boolean hasPrevious = false;
    private int previous = -1;
    private long previousTime;

    public void clear() {
        this.available = false;
        this.hasPrevious = false;
    }

    public void set(int value) {
        if (available) {
            previous = this.value;
            previousTime = System.currentTimeMillis();
            hasPrevious = true;
        } else available = true;
        if (value < 0) value = 0;
        this.value = value;
    }

    public int get() {
        if (!available) throw new IllegalStateException("TransformStart is not set");
        return value;
    }

    public void increment() {
        set(get() + 1);
    }

    public boolean isAvailable() {
        return available;
    }

    public boolean hasPrevious() {
        return hasPrevious;
    }

    public int getPrevious() {
        if (!hasPrevious) throw new IllegalStateException("TransformStart has no previous");
        return previous;
    }

    public boolean showPrevious() {
        return hasPrevious() && previousTime >= System.currentTimeMillis() - 500;
    }
}
