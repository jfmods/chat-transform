package io.gitlab.jfronny.chattransform.client.mixin;

import io.gitlab.jfronny.chattransform.Cfg;
import io.gitlab.jfronny.chattransform.ChatTransform;
import io.gitlab.jfronny.chattransform.client.ITextFieldWidget;
import net.minecraft.client.gui.screen.ChatScreen;
import net.minecraft.client.gui.widget.TextFieldWidget;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.*;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

@Mixin(ChatScreen.class)
public abstract class ChatScreenMixin {
    @Shadow protected TextFieldWidget chatField;

    @Inject(at = @At("RETURN"), method = "init()V")
    void init(CallbackInfo ci) {
        if (Cfg.Client.mode == Cfg.Mode.Live) ((ITextFieldWidget) chatField).chattransform$activate();
    }

    @Redirect(method = "keyPressed(III)Z", at = @At(value = "INVOKE", target = "Lnet/minecraft/client/gui/widget/TextFieldWidget;getText()Ljava/lang/String;"))
    String finalizeTransforms(TextFieldWidget instance) {
        String text = ((ITextFieldWidget) chatField).chattransform$finalize();
        if (Cfg.Client.mode == Cfg.Mode.OnSend) text = ChatTransform.transform(text);
        return text;
    }
}
