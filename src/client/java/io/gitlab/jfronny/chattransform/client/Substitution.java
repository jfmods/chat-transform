package io.gitlab.jfronny.chattransform.client;

public record Substitution(int start, int end, long time) {
    public Substitution(int start, int end) {
        this(start, end, System.currentTimeMillis());
    }

    public boolean shouldShow() {
        return time >= System.currentTimeMillis() - 500;
    }
}
