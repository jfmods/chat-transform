package io.gitlab.jfronny.chattransform;

import io.gitlab.jfronny.commons.logger.SystemLoggerPlus;

public class ChatTransform {
    public static final String MOD_ID = "chat-transform";
    public static final SystemLoggerPlus LOG = SystemLoggerPlus.forName(MOD_ID);

    public static String transform(String text) {
        return Cfg.General.substitutions.asSubstitution().apply(text);
    }
}
