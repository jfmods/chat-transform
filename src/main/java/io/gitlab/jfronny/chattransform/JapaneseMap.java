package io.gitlab.jfronny.chattransform;

import io.gitlab.jfronny.commons.throwable.ThrowingBiConsumer;

public class JapaneseMap {
    public static final String katakanaTable = """
            	a	i	u	e	o	n
            	ア	イ	ウ	エ	オ	ン
            x	ァ	ィ	ゥ	ェ	ォ
            k	カ	キ	ク	ケ	コ
            ky	キャ		キュ		キョ
            s	サ		ス	セ	ソ
            sh	シャ	シ	シュ	シェ	ショ
            t	タ	ティ	ツ	テ	ト
            ts			ツ
            ch	チャ	チ	チュ	チェ	チョ
            n	ナ	ニ	ヌ	ネ	ノ
            ny	ニャ		ニュ		ニョ
            h	ハ	ヒ	フ	ヘ	ホ
            hy	ヒャ		ヒュ		ヒョ
            f	ファ	フィ	フ	フェ	フォ
            m	マ	ミ	ム	メ	モ
            my	ミャ		ミュ		ミョ
            y	ヤ		ユ		ヨ
            r	ラ	リ	ル	レ	ロ
            ry	リャ	リィ	リュ	リェ	リョ
            w	ワ	ウィ		ウェ	ヲ
            g	ガ	ギ	グ	ゲ	ゴ
            gy	ギャ		ギュ		ギョ
            z	ザ		ズ	ゼ	ゾ/ヂョ
            j	ジャ/ヂャ	ジ/ヂ	ジュ/ヂュ	ジェ	ジョ
            d	ダ		ヅ	デ	ド
            b	バ	ビ	ブ	ベ	ボ
            by	ビャ		ビュ		ビョ
            p	パ	ピ	プ	ペ	ポ
            py	ピャ		ピュ		ピョ
            v			ゔ""";

    public static final String hiraganaTable = """
            	a	i	u	e	o	n
            	あ	い	う	え	お	ん
            x	ぁ	ぃ	ぅ	ぇ	ぉ
            k	か	き	く	け	こ
            ky	きゃ		きゅ		きょ
            s	さ		す	せ	そ
            sh	しゃ	し	しゅ		しょ
            t	た		つ	て	と
            ts			つ
            ch	ちゃ	ち	ちゅ	ちぇ	ちょ
            n	な	に	ぬ	ね	の
            ny	にゃ		にゅ		にょ
            h	は	ひ	ふ	へ	ほ
            hy	ひゃ		ひゅ		ひょ
            f			ふ
            m	ま	み	む	め	も
            my	みゃ		みゅ		みょ
            y	や		ゆ		よ
            r	ら	り	る	れ	ろ
            ry	りゃ	りぃ	りゅ	りぇ	りょ
            w	わ	ゐ		ゑ	を
            g	が	ぎ	ぐ	げ	ご
            gy	ぎゃ		ぎゅ		ぎょ
            z	ざ		ず	ぜ	ぞ/ぢょ
            j	じゃ/ぢゃ	じ/ぢ	じゅ/ぢゅ		じょ
            d	だ		づ	で	ど
            b	ば	び	ぶ	べ	ぼ
            by	びゃ		びゅ		びょ
            p	ぱ	ぴ	ぷ	ぺ	ぽ
            py	ぴゃ		ぴゅ		ぴょ
            v			ゔ""";

    public static <TEx extends Exception> void fromTable(String table, ThrowingBiConsumer<String, String, TEx> target) throws TEx {
        String[] rows = table.split("\n");
        String[] colNames = rows[0].split("\t");
        for (int i = 1; i < rows.length; i++) {
            String row = rows[i];
            String[] cols = row.split("\t");
            String rowName = cols[0];
            for (int j = 1; j < cols.length; j++) {
                String kana = cols[j];
                String value = rowName + colNames[j];
                String[] kanas = kana.split("/");
                for (String singleKana : kanas) {
                    if (!singleKana.isEmpty()) {
                        target.accept(value, singleKana);
                    }
                }
            }
        }
    }
}
