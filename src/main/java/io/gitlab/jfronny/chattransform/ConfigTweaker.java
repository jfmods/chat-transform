package io.gitlab.jfronny.chattransform;

import io.gitlab.jfronny.commons.data.String2ObjectMap;
import io.gitlab.jfronny.commons.serialize.SerializeWriter;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeAdapter;
import io.gitlab.jfronny.commons.serialize.databind.api.TypeToken;
import io.gitlab.jfronny.commons.serialize.json.JsonWriter;
import io.gitlab.jfronny.commons.throwable.ThrowingFunction;
import io.gitlab.jfronny.libjf.LibJf;
import io.gitlab.jfronny.libjf.config.api.v2.ConfigHolder;
import io.gitlab.jfronny.libjf.config.api.v2.Naming;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.CategoryBuilder;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.ConfigBuilder;
import io.gitlab.jfronny.libjf.config.api.v2.dsl.Migration;
import net.fabricmc.loader.api.FabricLoader;
import net.minecraft.text.Text;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

public class ConfigTweaker {
    private static final Path dir = FabricLoader.getInstance().getConfigDir().resolve(ChatTransform.MOD_ID);
    private static final TypeAdapter<String2ObjectMap<String>> substitutionsAdapter = LibJf.MAPPER.getAdapter(new TypeToken<>() {});
    private static final Path path = dir.resolve("config.json");

    static {
        try {
            if (!Files.exists(dir)) {
                Files.createDirectories(dir);
                General.writeDefaultPresets();
            }
        } catch (IOException e) {
            ChatTransform.LOG.error("Failed to create config directory", e);
        }
        ConfigHolder.getInstance().migrateFiles(ChatTransform.MOD_ID);
        var oldPath = FabricLoader.getInstance().getConfigDir().resolve(ChatTransform.MOD_ID + ".json5");
        if (!Files.exists(path) && Files.exists(oldPath)) {
            try {
                Files.move(oldPath, path);
            } catch (IOException e) {
                ChatTransform.LOG.error("Failed to move config file", e);
            }
        }
    }

    public static ConfigBuilder<?> tweak(ConfigBuilder<?> builder) {
        return builder.addMigration("substitutions", Migration.of(reader -> {
            Cfg.General.substitutions = substitutionsAdapter.deserialize(reader);
        })).setPath(path);
    }

    public static class General {
        private static <TEx extends Exception, Writer extends SerializeWriter<TEx, Writer>> Writer owo(Writer writer) throws TEx {
            writer.name("r").value("w");
            writer.name("l").value("w");
            writer.name("R").value("W");
            writer.name("L").value("W");
            writer.name("no").value("nu");
            writer.name("has").value("haz");
            writer.name("have").value("haz");
            writer.name("you").value("uu");
            writer.name("the ").value("da ");
            writer.name("The ").value("Da ");
            writer.name("This").value("Dis");
            writer.name("this").value("dis");
            writer.name("That").value("Dat");
            writer.name("that").value("dat");
            return writer;
        }

        private static final char[] consonants = "bcdfghjklmprstwz".toCharArray();
        private static <TEx extends Exception, Writer extends SerializeWriter<TEx, Writer>> Writer katakana(Writer writer) throws TEx {
            writer.name("-").value("ー");
            for (char c : consonants) {
                writer.name("" + c + c).value("ッ" + c);
            }
            writer.name("nn").value("ン");
            JapaneseMap.fromTable(JapaneseMap.katakanaTable, (k, v) -> writer.name(k).value(v));
            return writer;
        }

        private static <TEx extends Exception, Writer extends SerializeWriter<TEx, Writer>> Writer hiragana(Writer writer) throws TEx {
            writer.name("-").value("ー");
            for (char c : consonants) {
                writer.name("" + c + c).value("っ" + c);
            }
            writer.name("nn").value("ん");
            JapaneseMap.fromTable(JapaneseMap.hiraganaTable, (k, v) -> writer.name(k).value(v));
            return writer;
        }

        private static void write(String name, ThrowingFunction<JsonWriter, JsonWriter, IOException> generate) {
            try (var writer = Files.newBufferedWriter(dir.resolve(name + ".preset.json"));
                 var jw = LibJf.LENIENT_TRANSPORT.createWriter(writer)) {
                generate.apply(jw.beginObject()).endObject();
            } catch (IOException e) {
                ChatTransform.LOG.error("Failed to save preset " + name, e);
            }
        }

        public static void writeDefaultPresets() {
            write("owo", General::owo);
            write("katakana", General::katakana);
            write("hiragana", General::hiragana);
        }

        public static CategoryBuilder<?> tweak(CategoryBuilder<?> builder) {
            try (Stream<Path> possible = Files.list(dir)) {
                possible.filter(path -> path.getFileName().toString().endsWith(".preset.json"))
                        .filter(Files::isRegularFile)
                        .forEach(path -> builder.addPreset(
                                path.getFileName().toString().replace(".preset.json", ""),
                                () -> {
                                    try (var isr = Files.newBufferedReader(path);
                                         var jsr = LibJf.LENIENT_TRANSPORT.createReader(isr)) {
                                        Cfg.General.substitutions = substitutionsAdapter.deserialize(jsr);
                                    } catch (IOException e) {
                                        ChatTransform.LOG.error("Failed to load preset " + path, e);
                                    }
                                }
                        ));
            } catch (IOException e) {
                ChatTransform.LOG.error("Failed to load presets", e);
            }
            return builder;
        }
    }

    public static class CNaming extends Naming.Delegate implements Naming.Custom {
        public CNaming() {
            super(ChatTransform.MOD_ID);
        }

        @Override
        public String getId() {
            return ChatTransform.MOD_ID;
        }

        @Override
        public Naming category(String name) {
            Naming dlg = delegate.category(name);
            if (name.equals("general")) {
                return new Naming.Delegate(dlg) {
                    @Override
                    public Text preset(String name) {
                        return Text.translatableWithFallback(ChatTransform.MOD_ID + ".jfconfig." + name, name);
                    }
                };
            }
            return dlg;
        }
    }
}
