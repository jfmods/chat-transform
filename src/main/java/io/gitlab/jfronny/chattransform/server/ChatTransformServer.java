package io.gitlab.jfronny.chattransform.server;

import com.mojang.brigadier.Command;
import io.gitlab.jfronny.chattransform.Cfg;
import io.gitlab.jfronny.chattransform.ChatTransform;
import io.gitlab.jfronny.libjf.config.api.v2.ConfigInstance;
import net.fabricmc.api.DedicatedServerModInitializer;
import net.fabricmc.fabric.api.command.v2.CommandRegistrationCallback;
import net.fabricmc.fabric.api.message.v1.ServerMessageDecoratorEvent;
import net.fabricmc.fabric.api.networking.v1.ServerPlayConnectionEvents;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.*;

import java.util.Arrays;
import java.util.Objects;

import static net.minecraft.server.command.CommandManager.literal;

public class ChatTransformServer implements DedicatedServerModInitializer {
    public static final ConfigInstance CONFIG_INSTANCE = Objects.requireNonNull(ConfigInstance.get(ChatTransform.MOD_ID));

    @Override
    public void onInitializeServer() {
        CommandRegistrationCallback.EVENT.register((dispatcher, registryAccess, environment) -> {
            var root = literal(ChatTransform.MOD_ID).executes(context -> {
                context.getSource().sendMessage(Text.literal(ChatTransform.MOD_ID + " is " + (get(context.getSource().getPlayer()) ? "enabled" : "disabled") + " for you."));
                return Command.SINGLE_SUCCESS;
            });
            root.then(literal("opt-in").requires(src -> Cfg.Server.playerConfigurable).executes(context -> {
                if (context.getSource().getPlayer() == null) {
                    context.getSource().sendError(Text.literal("You are not a player."));
                    return Command.SINGLE_SUCCESS;
                }
                if (get(context.getSource().getPlayer())) {
                    context.getSource().sendError(Text.literal("You are already opted in to using chat-transform."));
                    return Command.SINGLE_SUCCESS;
                }
                set(context.getSource().getPlayer(), true);
                context.getSource().sendMessage(Text.literal("Toggled " + ChatTransform.MOD_ID + " on"));
                return Command.SINGLE_SUCCESS;
            }));
            root.then(literal("opt-out").requires(src -> Cfg.Server.playerConfigurable).executes(context -> {
                if (context.getSource().getPlayer() == null) {
                    context.getSource().sendError(Text.literal("You are not a player."));
                    return Command.SINGLE_SUCCESS;
                }
                if (!get(context.getSource().getPlayer())) {
                    context.getSource().sendError(Text.literal("You are already opted out of using chat-transform."));
                    return Command.SINGLE_SUCCESS;
                }
                set(context.getSource().getPlayer(), false);
                context.getSource().sendMessage(Text.literal("Toggled " + ChatTransform.MOD_ID + " off"));
                return Command.SINGLE_SUCCESS;
            }));
            root.then(literal("reset").requires(src -> Cfg.Server.playerConfigurable).executes(context -> {
                if (context.getSource().getPlayer() == null) {
                    context.getSource().sendError(Text.literal("You are not a player."));
                    return Command.SINGLE_SUCCESS;
                }
                reset(context.getSource().getPlayer());
                context.getSource().sendMessage(Text.literal("Reset " + ChatTransform.MOD_ID + " to " + (Cfg.Server.defaultEnable ? "on" : "off")));
                return Command.SINGLE_SUCCESS;
            }));
            root.then(literal("reset-all").requires(src -> src.hasPermissionLevel(4)).executes(context -> {
                Cfg.Server.playerStates.clear();
                CONFIG_INSTANCE.write();
                context.getSource().sendMessage(Text.literal("Reset all player states"));
                return Command.SINGLE_SUCCESS;
            }));
            dispatcher.register(root);
        });

        ServerPlayConnectionEvents.JOIN.register((handler, sender, server) -> {
            if (Cfg.Server.messageOnFirstConnect && !has(handler.getPlayer())) {
                set(handler.getPlayer(), Cfg.Server.defaultEnable);
                MutableText text = Text.literal("This server uses " + ChatTransform.MOD_ID + " to transform messages you send.");
                if (Cfg.Server.playerConfigurable) {
                    text.append(" To turn it " + (Cfg.Server.defaultEnable ? "off" : "on") + ", use ");
                    String command = "/" + ChatTransform.MOD_ID + " opt-" + (Cfg.Server.defaultEnable ? "out" : "in");
                    text.append(Text.literal(command)
                            .styled(s -> s.withClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, command)))
                    );
                    text.append(".");
                }
                handler.getPlayer().sendMessage(text);
            }
        });

        ServerMessageDecoratorEvent.EVENT.register(ServerMessageDecoratorEvent.CONTENT_PHASE, (sender, message) -> {
            return sender != null && get(sender) ? transform(message) : message;
        });
    }

    private boolean has(ServerPlayerEntity player) {
        return Cfg.Server.playerStates.containsKey(player.getUuidAsString());
    }

    private boolean get(ServerPlayerEntity player) {
        return Cfg.Server.playerStates.computeIfAbsent(player.getUuidAsString(), k -> Cfg.Server.defaultEnable);
    }

    private void set(ServerPlayerEntity player, boolean value) {
        Cfg.Server.playerStates.put(player.getUuidAsString(), value);
        CONFIG_INSTANCE.write();
    }

    private void reset(ServerPlayerEntity player) {
        Cfg.Server.playerStates.remove(player.getUuidAsString());
        CONFIG_INSTANCE.write();
    }

    private Text transform(Text source) {
        MutableText result = MutableText.of(transform(source.getContent()))
                .setStyle(source.getStyle());
        for (Text sibling : source.getSiblings()) {
            result.append(transform(sibling));
        }
        return result;
    }

    private TextContent transform(TextContent source) {
        if (source instanceof TranslatableTextContent tx) {
            Object[] args = tx.getArgs();
            args = Arrays.copyOf(args, args.length);
            for (int i = 0; i < args.length; i++) {
                if (args[i] instanceof Text tx1) args[i] = transform(tx1);
                else if (args[i] instanceof TextContent tx1) args[i] = transform(tx1);
                else if (args[i] instanceof String tx1) args[i] = ChatTransform.transform(tx1);
                else args[i] = args[i];
            }
            return new TranslatableTextContent(tx.getKey(), ChatTransform.transform(tx.getFallback()), args);
        } else if (source instanceof PlainTextContent.Literal tx) {
            return new PlainTextContent.Literal(ChatTransform.transform(tx.string()));
        } else {
            return source;
        }
    }
}
