package io.gitlab.jfronny.chattransform;

import io.gitlab.jfronny.commons.data.String2ObjectMap;
import io.gitlab.jfronny.libjf.config.api.v2.*;

import java.util.HashMap;
import java.util.Map;

@JfConfig(tweaker = ConfigTweaker.class)
public class Cfg {
    @Category(tweaker = ConfigTweaker.General.class)
    public static class General {
        @Entry public static String2ObjectMap<String> substitutions = new String2ObjectMap<>();
    }

    @Category
    public static class Client {
        @Entry public static Mode mode = Mode.Live;
        @Entry public static boolean visualize = true;
    }

    @Category
    public static class Server {
        @Entry public static boolean messageOnFirstConnect = true;
        @Entry public static Map<String, Boolean> playerStates = new HashMap<>();
        @Entry public static boolean playerConfigurable = true;
        @Entry public static boolean defaultEnable = true;
    }

    public enum Mode {
        Live, OnSend
    }
}
