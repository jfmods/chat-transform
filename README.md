Chat-Transform allows you to easily select text to replace in chat messages.
It comes preconfigured with replacements for romaji->hiragana and romaji->katakana transformation as well as good old owoification.
Replacements are applied to your messages as you type them or, if you prefer that, when you send them.